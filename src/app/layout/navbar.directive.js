// Configure a new directive
(function() {
  'use strict';
  
  angular
    .module('app.layout')
    .directive('custNavbar', custNavbar);
  
  function custNavbar() {
    return {
      templateUrl: 'app/layout/navbar.html',
      restrict: 'E',// Restrict to be an element only (E)
      scope: {},
      controller: NavbarController,
      controllerAs: 'vm'
    };
  }
  
  NavbarController.$inject = ['$location', 'authService'];
  
  function NavbarController($location, authService) {
    var vm = this;
    
    vm.isLoggedIn = authService.isLoggedIn;
    vm.logout = logout;
    
    function logout() {
      console.log('Logging out.');
      authService.logout();
      $location.path('/');
    }
  }
  
}())