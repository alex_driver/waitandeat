(function(){
  'use strict';
  
  // Access the module this way, without the dependencies array
  angular
    .module('app.landing')
    .config(configFunction);
  
  // Make anything in the array available to the configFunction
  configFunction.$inject = ['$routeProvider'];
  
  function configFunction($routeProvider){
    $routeProvider.when('/', {
      templateUrl: 'app/landing/landing.html'
    });
  }
  
})();