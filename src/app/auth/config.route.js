(function(){
  'use strict';
  
  // Access the module this way, without the dependencies array
  angular
    .module('app.auth')
    .config(configFunction);
  
  // Make anything in the array available to the configFunction
  configFunction.$inject = ['$routeProvider'];
  
  function configFunction($routeProvider){
    $routeProvider
      .when('/register', {
        templateUrl: 'app/auth/register.html',
        controller: 'AuthController',
        controllerAs: 'vm'
      });
    $routeProvider
      .when('/login', {
        templateUrl: 'app/auth/login.html',
        controller: 'AuthController',
        controllerAs: 'vm'
      });
  }
  
})();