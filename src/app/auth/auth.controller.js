(function() {
  'use strict';
  
  angular
    .module('app.auth')
    .controller('AuthController', AuthController);
  
  // Inject services into the controller
  AuthController.$inject = ['$location', 'authService'];
  
  // Injected services = arguments in the controller func, MUST be in the same order as injected
  function AuthController($location, authService) {
    var vm = this;
    
    vm.user = {
      email: '',
      password: ''
    };
    
    // Expose methods to the vm in the view
    
    vm.register = register;
    vm.login = login;
    
    // Define the methods
    
    function register(user) {
      return authService.register(user)
        .then(function () { // After successful register, login.
          vm.login(user);
        })
        .then(function(){
          return authService.sendWelcomeEmail(user.email);
        })
        .catch(function (error) {
          vm.error = error;
        });
    }
    
    function login(user) {
      return authService.login(user)
        .then(function(loggedInUser) {
          console.log(loggedInUser);
          console.log('Redirecting the user to /waitlist');
          $location.path('/waitlist'); // Use $location service to redirect the user
        })
        .catch(function(error) {
          vm.error = error;
        });
    }
    
  }
}());