(function(){
  'use strict';

  angular
    .module('app.waitList')
    .directive('custPartyTable', custPartyTable);

  function custPartyTable() {
  	return {
      templateUrl: 'app/waitList/directives/partyTable.html',
      restrict: 'E',
      controller: PartyTableController,
      controllerAs: 'vm',
      bindToController: true, // binds values from isolate scope to controller instance
      scope: { // bindToController: true allows our controller access to the attributes defined here
        parties: '='
      }
  	};
  }  

  PartyTableController.$inject = ['textMessageService'];

  function PartyTableController(textMessageService) {
    var vm = this;

    vm.removeParty = removeParty;
    vm.sendTextMessage = sendTextMessage;
    vm.toggleDone = toggleDone;

    function removeParty(party) {
      vm.parties.$remove(party);
    }

    function sendTextMessage(party) {
      textMessageService.sendTextMessage(party, vm.parties);
    }

    function toggleDone(party) {

    }
  }
}());