(function(){
  'use strict';
  
  angular
    .module('app.waitList')
    .config(configFunction);
  
  configFunction.$inject = ['$routeProvider'];
  
  function configFunction($routeProvider) {
    $routeProvider.when('/waitlist', {
      templateUrl: 'app/waitList/waitList.html',
      controller: 'WaitListController',
      controllerAs: 'vm',
      resolve: {user: resolveUser}
    });
  }
  
  resolveUser.$inject = ['authService'];
  
  function resolveUser(authService) {
    // if user logged in, returns logged in user
    // otherwise rejects promise - if this failure occurs, an error event will be fired in the root scope
    // see the run function in app.module.js to see how we handle this error event
    return authService.firebaseAuthObject.$requireSignIn();
  }
  
})();